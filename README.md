# Docker iamge for mosquitto client

As a mosquitto client is not installed on official image, create docker image for mosquitto client.

# Build

```
cd [WORKDIR]
git clone git@gitlab.com:ingktds/mosquitto-client.git
cd mosquitto-client
docker build -t mosquitto-client .
```

# Run MQTT Broker

```
docker run -d -p 1883:1883 --rm --name mqtt-broker eclipse-mosquitto
```

# Run Subscriber

```
docker run -it --rm --name mqtt-subscriber --link mqtt-broker:mb ingktds/mosquitto-client mosquitto_sub -h mb -t 'test_topic'
```

# Run Publisher

```
docker run -d --rm --name mqtt-publisher --link mqtt-broker:mb ingktds/mosquitto-client mosquitto_pub -h mb -t 'test_topic' -m 'Hello World!'
```
